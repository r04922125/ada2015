#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;
// first 10 cases
#define maxVertices 500
#define maxEdges 50000

class Edge{
private:
public:
	static int verticesNumber;
	static int edgesNumber;
	int from, to, weight;
	void setInitial(int f, int t, int w){from = f; to=t; weight=w;}
	void printGraph(){cout<<"edges: "<<from<<"->"<<to<<"="<<weight<<endl;}
};

void merge(int left, int right);
void mergeSort(int left, int right);
int find(int x);
void un(int x, int y);
long long int kruskal(int skip);
int popList(Edge *ptr, int i);
void MUST();

int *p;
Edge *num;
Edge *graph;
long long int  minCost, needCost;
int v,e,needEdges;

int main(){
	int t=0, i =0;
	int from=0, to=0, edge=0;
	cin>>t;
	while (t--){
		minCost = 0, needCost=0, needEdges =0;
		cin>>v>>e;
		// cout<<"vertices: "<<v<<"//edges: "<<e<<endl;
		num 	= new Edge[e];
		graph  = new Edge[v-1];
		
		// get edge information and store in class 
		for (i=0;i<e;i++){
			cin>>from>>to>>edge;
			num[i].setInitial(from,to,edge);
		}
		// Sort the edge weight
		mergeSort(0, e-1);
		// for (i=0;i<e;i++){num[i].printGraph();}


		// cout <<"After mergeSort"<<endl;
		minCost = kruskal(-1);
		// cout << "After kruskal"<<endl;
		// for (i=0;i<v-1;i++){graph[i].printGraph();}
		// cout << "minCost= "<<minCost<<endl;
		MUST();
		// cout<<"NeedCost = "<<needCost<<", NeedEdges= "<<needEdges<<endl;
		cout<<needEdges<<" "<<needCost<<endl;


		delete p; delete graph; delete num;
	}
}

// ---subfunction 
int popList(Edge *ptr, int i){
	int count=1;
	// int temp=ptr[i].weight;
	while(ptr[i++].weight==ptr[i].weight){count++;}
	return count;
}
void MUST(){
	int g_Num =0, e_Num=0;
	while(g_Num<v-1 && e_Num<e-1){
		int tG = popList(graph,g_Num);
		int tE = popList(num,e_Num);
		if(tG==tE){
			// cout<<"tG==tE"<<endl;
			needCost += (tG*num[e_Num].weight);
			needEdges += tG;

		}else{
			while(tE--){
				int temp = kruskal(e_Num);
				// cout<<"tE= "<<tE<<"e_Num= "<<e_Num<<endl;
				if(minCost<temp && temp>0){needCost += num[e_Num].weight; needEdges++;}
			}
		}
		// cout<<"NeedCost = "<<needCost<<", NeedEdges= "<<needEdges<<endl;
		// cout<<"G= "<<g_Num<<", E= "<<e_Num<<endl;
		// cout<<"tG= "<<tG<<", tE= "<<tE<<endl;
		g_Num +=tG;
		e_Num +=tE;

	}

}

long long int kruskal(int skip){
	// eA add edge to graph//eN queue to add edge
	int eA=0, eN=0, i=0, cost=-1;
	p = new int[v+1];
	for (i=1; i<=v; ++i) {p[i] = i;}
	while(eA< v-1){
		// cout<<eA<<"//"<<eN<<endl;
		// if(checkList()) {continue;}	
		if(eN==e){ 
			// cout<<"Graph is not connected"; 
			cost=-1;
			break;
		}
		if(eN==skip){eN++;continue;}
		if(find(num[eN].from)==find(num[eN].to)) {eN++; continue;}
		// for (i=0; i<verticesNumber; ++i) {cout <<"int"<<i<<"("<<p[i]<<")"<<endl;}
		// cout<<"i'm here"<<endl;
		un(num[eN].from, num[eN].to);
		cost += num[eN].weight;
		graph[eA++].setInitial(num[eN].from, num[eN].to, num[eN].weight);
		eN++;
	} 
	delete p;
	return cost;
}

// disjoint set 
int find(int x)  {return x == p[x] ? x : (p[x] = find(p[x]));}
void un(int x, int y) {p[find(x)] = find(y);}
// sorting method
void merge(int left, int right)
{
	// cout << "into merge"<< left <<"+"<<right<< endl;
	Edge *ptr=new Edge[right-left];
	int i, j, k;
	int mid = (left + right) / 2;
	for (k=0, i=left, j=mid+1;i<=mid||j<=right;k++){
		// cout <<"mergeStart"<<endl;
		if( i>mid ) /* 左邊超過中間 */
			ptr[k] = num[j++];
		else if ( j>right ) /* 右邊超過邊界 */ 
			ptr[k] = num[i++];
		else if ( num[i].weight<num[j].weight)
		// else if ( num[i]>num[j] ) /* 判斷兩邊值的大小：左>右*/ 
			ptr[k] = num[i++];
		else 
			ptr[k] = num[j++];
	}
	// cout<<"finish mergeSelect"<<endl;
	/*確認*/
	for (i = left, k =0 ; i<=right ; i++,k++){
		num[i] = ptr[k];
	}
	// cout << "finish ptr =num, jump out"<<endl;
	delete ptr;
}
void mergeSort(int left, int right)
{
	int mid = (left + right)/2;
	// cout<<"into mergeSort"<<left<<"++"<<right<<"="<<mid<<endl;
	if (left < right ){
		// cout <<"into first mergeSort"<<endl;
		mergeSort(left,mid);
		// cout << "into second mergeSort"<<endl;
		mergeSort(mid+1, right);
		// cout << "Combine the result"<<endl;
		merge(left,  right);
		// cout << "Finish"<<endl;
	}
}
