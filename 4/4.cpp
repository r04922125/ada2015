#include <stdio.h>
#include <stdlib.h>

/*improve:
1. 七的快速除法
2. 儲存上次的結果，彼此的差，即為新求的部份(如何去判斷現值及新求的值)
3. 從三位數推到四位數，及之後的DP方法
*/
long long int Jud7=0,Jud4 = 0, lucky = 0;

void que7(long long int a){
	int temp = a;
	int aR,aQ;
	aR = temp%10;
	aQ = temp/10;
	if (aR == 7){
		Jud7++;
	}else if (aR == 4){
		Jud4++;
	}
	if(aQ==0){
		if (Jud7>=3){
			if(Jud7-Jud4>0){
				lucky++;
			}
		}
	}else {
		que7(aQ);
	}
}

int main(){	
	long long int a,b,i,t;
	long long int aQ,aR;
	scanf("%lld",&t);
	while(t--){
		scanf("%lld%lld",&a,&b);
		for (i=a;i<=b;i++){			
				que7(i);
				Jud7 = 0;
				Jud4 = 0;
			
		}
		printf("%lld", lucky);
		printf("\n");
		lucky = 0;
	}
}
