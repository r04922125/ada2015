#include <stdio.h>
#include <stdlib.h>
long long int n,c,e,p, *ptr, *num, *equ;
float dv;
void merge(long long int, long long int);
void mergeSort(long long int, long long int);
bool judge(long long int,long long int);
long long int  pow(long long int,long long int);

int main(){
	long long int t;
	long long int i, j, temp;
	scanf("%lld", &t);
	while(t--){
		scanf("%lld%lld%lld%lld", &n, &c, &e, &p);
		ptr = (long long int *) malloc(n * sizeof(long long int));
		num = (long long int *) malloc(n * sizeof(long long int));
		equ = (long long int *) malloc(2*n * sizeof(long long int));
		for (i=1;i<=n;i++)
		{
			ptr[i-1] = i;
		}
		e = e % (p-1);
		dv = float(p)/2;
		for (i=2;i<=2*n;i++)
		{
			equ[i] = pow(i,e)%p;
		}

		mergeSort(0,n-1);
	
		for (i=1;i<=n;i++)
		{
			printf("%lld ", ptr[i-1]);
		}
		free (ptr);
		free (num);
		printf("\n");
	}

	return 0;
}

bool judge(long long int a, long long int b)
{
	long long int temp;
	temp = (((c * (a-b))%p) * (equ[a+b]%p))%p;
	//temp = (c*(a-b)%p*pow((a+b),e))%p; 

	if (temp<0)
		temp = temp + p;
	if(float(temp)>dv)
	{
		return false;
	}
	else{
		return true;
	}
}

void merge(long long int left, long long int right)
{
	long long int i, j, k;
	long long int mid = (left + right) / 2;
	for (k=0, i=left, j=mid+1;i<=mid||j<=right;k++){
		if( i>mid )
			num[k] = ptr[j++];
		else if ( j>right )
			num[k] = ptr[i++];
		else if ( !(judge(ptr[i], ptr[j]) ))
			num[k] = ptr[i++];
		else 
			num[k] = ptr[j++];
	}
	for (i = left, k =0 ; i<=right ; i++,k++){
		ptr[i] = num[k];
	}
}
void mergeSort(long long int left, long long int right)
{
	long long int mid = (left + right)/2;

	if (left < right ){
		mergeSort(left,mid);
		mergeSort(mid+1, right);
		merge(left,  right);
	}
}

long long int pow(long long int a, long long int b)
{
	long long int result = 1;

	while (b)
	{
		if (b&1)
		{
			result *= a;
			result = result % p;
		}
			b >>=1 ;
			a *= a;
			a = a%p;
	}

	return result;
}


