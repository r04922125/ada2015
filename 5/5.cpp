#include <stdio.h>
#include <stdlib.h>
int size = 150;
int *num;
void PutNumber(int , int );
int DownNumber(int, int );
void PrintArray(int , int );

int main(){
	int t,n;
	int *ptr;
	scanf("%d", &t);
	while(t--){
		scanf("%d", &n);
		ptr = (int *) malloc((n+1) * sizeof(int));
		ptr[0] = 0;
		for (int i = 1;i<=n;i++){
			scanf("%d", &ptr[i]);
		}

		int count =1;
		int k,flag;
		num = (int *) malloc (size * sizeof(int));
		for (k=0;k<=size;k++)	num[k] = 0;

		while (count<=n){	
			k=1;	
			if (ptr[count]<ptr[count-1]) {
				do{
					ptr[count-1] = DownNumber(ptr[count-1], k);
					k++;
				}while(ptr[count]<ptr[count-1]);
				
			}else if (ptr[count]==ptr[count-1]){
				while(k){					
					if (num[size-k]!=0){
						ptr[count-1] = DownNumber(ptr[count-1], k);
						break;
					}
					k++;
				}
				
			}
			PutNumber(ptr[count]-ptr[count-1], 1);
			PrintArray(count, n);
			count++;				
		}
		free(num);
		free(ptr);
		printf("\n");
	}

}

void PutNumber(int a , int back_num){
	int m  = size - back_num;
	int temp = a-(9-num[m]);
	if (temp>0){
		num[m] = 9;
		PutNumber(temp, back_num+1);
	}else if (temp<0){
		num[m] += a;
	}else{
		num[m] = 9;
	}
}
int DownNumber(int a, int back_num){
	int m = size-back_num;
	if (num[m] != 0){
		a -= num[m];
		num[m] = 0;
		if (num[m-1]==9){
			a = DownNumber(a,back_num+1)  ;
		}else{
			num[m-1] +=1;
			a++;
		}		
	}
	return a;
}
void PrintArray(int count, int n){
	int flag=0;
	for (int i =0;i<size;i++){
		if (num[i] != 0 || flag){
			flag = 1;
			printf("%d", num[i]);
		}
	}
	if (count <n)	printf(" ");

}
